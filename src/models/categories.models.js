'use strict';
const { dbConn } = require('../config/db.config');

//Category object create
var Categories = function(category) {
    this.title = category.title;
};

Categories.findCategories = function(ids, result) {
    dbConn.query("SELECT categories.id, categories.title FROM categories WHERE categories.id IN (?)", [ids], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('categories : ', res);
            result(null, res);
        }
    });
};
module.exports = Categories;
