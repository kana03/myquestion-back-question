'use strict';
const { dbConn } = require('../config/db.config');

//Question object create
var Questions = function(question) {
    this.content = question.content;
    this.id_category = question.id_category;
    this.id_difficulty = question.id_difficulty;
};

Questions.findQuestions = function(id_category, id_difficulty, result) {
    dbConn.query("SELECT questions.id, questions.content FROM questions INNER JOIN categories ON questions.id_category = categories.id INNER JOIN difficulties ON questions.id_difficulty = difficulties.id WHERE categories.title = ? AND difficulties.title = ?", [id_category, id_difficulty], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('questions : ', res);
            result(null, res);
        }
    });
};

module.exports = Questions;
