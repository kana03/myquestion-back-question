'use strict';
const { dbConn } = require('../config/db.config');

//Answer object create
var Answers = function(answer) {
    this.content = answer.content;
    this.id_category = answer.response;
    this.id_question = answer.id_question;
};

Answers.findAnswers = function(ids, result) {
    dbConn.query("SELECT answers.content, answers.response, answers.id_question FROM answers INNER JOIN questions ON answers.id_question = questions.id WHERE questions.id IN (?)", [ids], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('answers : ', res);
            result(null, res);
        }
    });
};
module.exports = Answers;
