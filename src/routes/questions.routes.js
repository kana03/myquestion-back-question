const express = require('express')
const router = express.Router()
const questionsController = require('../controllers/questions.controller');

// Retrieve all Questions
router.post('/question', questionsController.getData);

router.get('/question', questionsController.findQuestions);

router.get('/answer', questionsController.findAnswers);

router.get('/category', questionsController.findCategories);

module.exports = router
