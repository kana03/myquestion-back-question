'use strict';
const Questions = require('../models/questions.models');
const Answers = require('../models/answers.models');
const Categories = require('../models/categories.models');
let categoryValue = '';
let difficultyValue = '';
let getNumberQuestion = '';

exports.getData = function (req, res) {
    categoryValue = req.body.categoryValue;
    difficultyValue = req.body.difficultyValue;
    getNumberQuestion = req.body.getNumberQuestion;
    res.send(req.body);
}

exports.findQuestions = function (req, res) {
    Questions.findQuestions(categoryValue, difficultyValue,function(err, question) {
        if (err) res.send(err);
        const response = [];
        const string = JSON.stringify(question);
        const json =  JSON.parse(string);
        for (let i = 0; i < getNumberQuestion; i++) {
            response.push(json[i]);
        }
        res.status(200).json({ data: response });
    });
}

exports.findAnswers = function (req, res) {
    Answers.findAnswers(req.query.id_question,function(err, answer) {
        if (err) res.send(err);
        res.status(200).json({ data: answer });
    });
}

exports.findCategories = function (req, res) {
    Categories.findCategories(req.query.id_category,function(err, answer) {
        if (err) res.send(err);
        res.status(200).json({ data: answer });
    });
}
